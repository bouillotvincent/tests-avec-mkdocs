
//const output = document.getElementById("output");
//const code = document.getElementById("code");

// function addToOutput(s, code, outputTag) {
//     console.log('ligne 6', document.getElementById(code), document.getElementById(code).value, s)
//     var myvalue = document.getElementById(outputTag).value
//     if (myvalue === undefined) {
//         document.getElementById(outputTag).value = '>>>' + document.getElementById(code).value + '\n' + s + '\n';
//     } else {
//         document.getElementById(outputTag).value += '>>>' + document.getElementById(code).value + '\n' + s + '\n';
//     }
// }

// output.value = 'Initializing...\n';
// // init Pyodide
// async function main(){
//     await loadPyodide({ indexURL : 'https://cdn.jsdelivr.net/pyodide/v0.17.0/full/' });
//     output.value += 'Ready!\n';
// }
// let pyodideReadyPromise = main();

// async function evaluatePython(code, outputTag) {
//     await pyodideReadyPromise;
//     try {
//     let output = await pyodide.runPythonAsync(document.getElementById(code).value);
//     console.log('ici', document.getElementById(code).value, output)
//     addToOutput(output, code, outputTag);
//     } catch(err) {
//     addToOutput(err, code, outputTag);
//     }
// }

// function evaluatePython2(code, outputTag) {
//     try {
//     let output = pyodide.runPython(document.getElementById(code).value);
//     console.log('ici', document.getElementById(code).value, output)
//     addToOutput(output, code, outputTag);
//     } catch(err) {
//     addToOutput(err, code, outputTag);
//     }
// }

// function getTextArea(textAreaID, outputTag) {
//     var key = window.event.keyCode;

//     if (key === 13 && !(window.event.shiftKey)) {
//         //var code2 = document.getElementById(textAreaID).value;
//         //console.log(code2)
//         evaluatePython2(textAreaID, outputTag)
//         console.log('in textArea', document.getElementById(outputTag).value)
//         document.getElementById(textAreaID).value = document.getElementById(outputTag).value ; //document.getElementById(textAreaID).value ;
//     }
// }


function sleep(s){
    return new Promise(resolve => setTimeout(resolve, s));
  }
  
async function main(id) {
await loadPyodide({ indexURL : 'https://cdn.jsdelivr.net/pyodide/v0.17.0/full/' });

let namespace = pyodide.globals.get("dict")();

pyodide.runPython(`
    import sys
    import js
    from pyodide import console
    import __main__

    class PyConsole(console._InteractiveConsole):
        def __init__(self):
            super().__init__(
                __main__.__dict__,
                persistent_stream_redirection=False,
            )

        def banner(self):
            return f"Welcome to the Pyodide terminal emulator 🐍\\n{super().banner()}"


    js.pyconsole = PyConsole()
`, namespace);
namespace.destroy();

let ps1 = '>>> ', ps2 = '... ';

async function lock(){
    let resolve;
    let ready = term.ready;
    term.ready = new Promise(res => resolve = res);
    await ready;
    return resolve;
}

async function interpreter(command) {
    let unlock = await lock();
    try {
    term.pause();
    // multiline should be splitted (useful when pasting)
    for( const c of command.split('\n') ) {
        let run_complete = pyconsole.run_complete;
        try {
            const incomplete = pyconsole.push(c);
            term.set_prompt(incomplete ? ps2 : ps1);
            let r = await run_complete;
            if(pyodide.isPyProxy(r)){
            r.destroy();
            }
        } catch(e){
            if(e.name !== "PythonError"){
            term.error(e);
            throw e;
            }
        }
        run_complete.destroy();
    }
    } finally {
    term.resume();
    await sleep(10);
    unlock();
    }
}

let term = $(id).terminal(
    interpreter,
    {
    greetings: pyconsole.banner(),
    prompt: ps1,
    completionEscape: false,
    height: 200,
    completion: function(command, callback) {
        callback(pyconsole.complete(command).toJs()[0]);
    }
    }
);


window.term = term;
pyconsole.stdout_callback = s => term.echo(s, {newline : false});
pyconsole.stderr_callback = s => {
    term.error(s.trimEnd());
}
term.ready = Promise.resolve();
pyodide._module.on_fatal = async (e) => {
    term.error("Pyodide has suffered a fatal error. Please report this to the Pyodide maintainers.");
    term.error("The cause of the fatal error was:");
    term.error(e);
    term.error("Look in the browser console for more details.");
    await term.ready;
    term.pause();
    await sleep(15);
    term.pause();
};
}

window.console_ready = main('#demo2');
window.console_ready = main('#demo');
