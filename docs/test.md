# Chapitre 12 : Traitement de données

## Introduction


<div onclick='start_term("id")' id="fake_id">
<label class="terminal"><span>>>> </span></label>
</div>
<div id="id" class="hide"></div>

## partie 2
<div onclick='start_term("id2")' id="fake_id2"><label class="terminal"><span>>>> </span></label></div><div id="id2" class="hide"></div>

## partie 3

{{terminal()}}

!!! {{exercice()}}

    === "Énoncé"

        ${1: enonce}

    === "Tips"

        ${1: enonce}

  	=== "Solutions"

        ${2: sol}


