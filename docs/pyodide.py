import sys
import js
from pyodide import console
import __main__

class PyConsole(console._InteractiveConsole):
    def __init__(self):
        super().__init__(
            __main__.__dict__,
            persistent_stream_redirection=False,
        )

    def banner(self):
        return f"Welcome to the Pyodide terminal emulator 🐍\\n{super().banner()}"


js.pyconsole = PyConsole()