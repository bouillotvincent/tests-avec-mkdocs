# Chapitre 11 : Traitement de données

## Bases de données


<!-- <p>You can execute any Python code. Just enter something in the box below and click the button.</p>
<input id='code' value='sum([1, 2, 3, 4, 5])'>
<textarea id='output' style='width: 100%;' rows='6' disabled></textarea>
<button onclick='evaluatePython("code","output")'>Run</button>
<div>
Output:
</div> -->

<!--<p id="demo"></p>-->

<!-- <textarea id='essai' style='width: 100%;' rows='6' onkeypress = 'getTextArea("essai","demo")'></textarea>

<p id="demo"></p> -->





## Dictionnaires Python

### Introduction

Pour représenter une base de données, utiliser des tableaux Python semble être une bonne idée. Mais, pour des bases de données importantes, on risque de se retrouver avec des données comme cela (cf Pronote) :

```python
[['Prénom;Né;Sexe;Classe;Entrée;Sortie;Option 1;Option 2;Option 3;Option 4;Option 5'],
['Eloise;2A03;G;1G03;02/09/2019;;ANGLAIS LV1;ITALIEN LV2;MATHEMATIQUES;Numérique et sciences informatiques; PHYSIQUE-CHIMIE'],
['Valentin;2003;G;1G03;02/09/2019;;ANGLAIS LV1;ITALIEN LV2;MATHEMATIQUES;Numérique et sciences informatiques; PHYSIQUE-CHIMIE'],
['Léo;2002;G;1G 08;02/09/2019;;ANGLAIS LV1;ALLEMAND LV2;Numérique et sciences informatiques;LITT. ANGLAIS; MATHEMATIQUES'],
['Thibaud;2003;G;1G03;02/09/2019;;ANGLAIS LV1;ESPAGNOL LV2;Numérique et sciences informatiques;MATHEMATIQUES; PHYSIQUE-CHIMIE'],
['Julia;2003;F;1G11;02/09/2019 ;;ANGLAIS LV1;ESPAGNOL LV2;LITT. ANGLAIS;Numérique et sciences informatiques; PHYSIQUE- CHIMIE'],
['Julien;2002;G;1G06;02/09/2019;;ANGLAIS LV1;ESPAGNOL LV2;Numérique et sciences informatiques;MATHEMATIQUES; LITT. ANGLAIS'],
['Sylvain;2003;G;1G09;02/09/2019;;ANGLAIS LV1;ESPAGNOL LV2;Numérique et sciences informatiques;LI.......................]
```

Imaginons : on souhaite accéder à l'Option 3 des élèves ayant anglais comme Option 1 ...

Je vais devoir dire : "quand le 6ème élément de la sous-liste est égal à 'ANGLAIS', lis le 8ème élément STP" ➠ Ignoble.

Il serait tellement plus facile de dire : "quand l'Option 1 est égal à ANGLAIS, lis l'Option 3".

!!! warning "A retenir"
    
    L'inconvénient des tableaux dans ce cas est qu'il est pénible d'y rechercher des **valeurs**. 
    
    En effet, ces valeurs sont repérées à partir de la notion d'**indices** et non sur la notion de contenu alors que les bases de données sont précisément basées sur la notion de contenu.

Pour faire une chose pareille, il faut nous appuyer sur une autre structure de données disponible en Python et basée sur les contenus : les dictionnaires.

### Manipulation de dictionnaires

Un tableau Python est une structure linéaire. Il est composé de **valeurs** repérées par des **indices**.

```python
mon_tableau = ["LaTaupe", "René", 2009]
```

Un dictionnaire Python est une structure composite. Il est composé de **valeurs** repérées par des **clés**.

```python
mon_dico = {"nom": "LaTaupe", "prenom": "René", "naissance": 2009}
```

!!! {{cours()}}

    - Un dictionnaire contient des couples **clés : valeurs**. 

    - Les clés ne peuvent pas apparaitre deux fois : elles sont uniques.

    - Un dictionnaire se caractérise par des accolades `#!py {}` ou `#!py <class dict`.

??? {{ exercice(0)}}

    === "Énoncé"

        Quelles sont les clés du dictionnaire Python suivant ?
        ```python
        mon_dico = {"Ville": "Paris", "Pays": "France", "Code": 0033}
        ```

    === "Aide"

        Un dictionnaire contient des couples **clés** : valeurs.

    === "Solution"

        Les clés sont ici `#!py "Ville"`, `#!py "Pays"` et `#!py "Code"`.
        ```python
        mon_dico = {"Ville": "Paris", "Pays": "France", "Code": 0033}
        ```


??? {{ exercice()}}

    === "Énoncé"

        Donnez les couples **clés : valeurs** du dictionnaire suivant.
        ```python
        mon_dico = {5: "orange", "jaune": "banane", 9: "abricot"}
        ```
    
    === "Aide"

        Pensez à l'ordre d'entrée dans un dictionnaire.

    === "Solution"
        Les clés sont ici `#!py 5`, `#!py "jaune"` et `#!py 9`.
        
        Les couples **clés : valeurs** sont donc `#!py "5: "orange"`, `#!py "jaune": "banane"` et `#!py 9: "abricot"`.
        ```python
        mon_dico = {5: "orange", "jaune": "banane", 9: "abricot"}
        ```

??? danger "Subtilités"

    Les clés doivent être immuables. 
    `#!py {[1,2,3]: 'immuable'}` renvoie le résultat suivant :
    ```bash
    Traceback (most recent call last):
    File "<stdin", line 1, in <module
    TypeError: unhashable type: 'list'
    ```

??? {{exercice()}}

    === "Énoncé"

        

    === "Aide"

        B

    === "Solutions"

        C



!!! {{cours()}}

    Il est possible d'initialiser un dictionnaire vide puis de le remplir avec les 4 lignes suivantes :

    - `#!py monDico = {}` # monDico = dict() est une autre syntaxe possible.

    - monDico["nom"] = "LaTaupe"

monDico["prenom"] = "René"

monDico["naissance"] = 2009


--- Exercice 1 ---
??? {{ exercice()}}

    === "Énoncé"

        A

    === "Aide"

        B

    === "Solution"

        C




Dans VSCodium, tapez le code suivant et exécutez-le :

monDico = {"nom": "LaTaupe", "prenom": "René", "naissance": 2009}
{{console(300)}}


print(f"Le type de mon Dico est {type(monDico)}")

print(monDico)

Quel est le type affiché ? \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Qu'avons-nous fait dans la première ligne ? \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

--- Exercice 2 ---

Dans VSCodium, créez un fichier appelé dicoTest.py, tapez le code suivant et exécutez-le:

monDico = {"nom": "LaTaupe", "prenom": "René", "naissance": 2009}

print( f'Bonjour je suis {monDico["prenom"]} {monDico["nom"]} !' )

Quel est le résultat attendu après l'exécution de ce programme ?

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Vérifiez votre réponse.

-   En réutilisant la même syntaxe, ajoutez la phrase : "Je suis né en ......" .

```{=html}
<!-- --
```
-   Testez votre programme en changeant le nom de René LaTaupe en Sergent LaTaupe.

Les dictionnaires sont des objets mutables (=pouvant être modifiés).

[Rappel :]{.ul}

-   les listes sont aussi des objets mutables et sont obtenus avec des crochets [] ;

-   les tuples sont des objets immutables obtenus à l'aide de parenthèses ().

--- Exercice 3 ---

Nous allons modifier le fichier dicoTest.py.

En suivant la méthode d'initialisation proposée page 8, rajoutez à votre dictionnaire monDico une clé "lieu_naissance" à laquelle on affectera la valeur "Copacabana".

Le code ressemblera donc à :

monDico = {"nom": "LaTaupe", "prenom": "René", "naissance": 2009}

\# instruction à rajouter :

\# ...... ...... ...... ...... ...... ...... ...... ...... ......

print( f'Bonjour je suis {monDico["prenom"]} {monDico["nom"]} ! Je suis né à {.................................} .' )

[Remarque :]{.ul} Il est maintenant clair qu'on peut accéder aux valeurs contenues dans un dictionnaire en tapant : nomDuDictionnaire["clé"] . C'est exactement ce que l'on souhaite faire pour lire du CSV.

--- Exercice 4 ---

Nous allons encore modifier le fichier dicoTest.py.

À la suite de René, créez un nouveau dictionnaire qui va s'appeler mesFruits :

mesFruits = {"poire": 3, "pomme": 4, "orange": 2}

Les dictionnaires sont mutables, donc modifiables.

-   J'aime bien les oranges. Soustraire 2 oranges à mesFruits en utilisant :

mesFruits["orange"] = mesFruits["orange"] - 2

-   Raccourcir cette instruction en évitant la répétition de mesFruits["orange"]

-   Afficher ensuite le nombre d'orange contenu dans mesFruits :

print("Il y a {...............} oranges dans le bol!" )

-   Normalement, vous avez du trouver qu'il n'y a plus d'oranges dans le bol... Supprimons (=[del]{.ul}ete) donc l'entrée orange. Pour faire cela, nous utilisons l'instruction [del]{.ul} :

del mesFruits["orange"]

Il est possible de parcourir un dictionnaire à l'aide d'une boucle for. Par rapport à une liste, ce parcours peut se faire selon les clés ou les valeurs.

Commençons par parcourir les clés à l'aide de la méthode keys().

--- Exercice 5 ---

-   À la suite de votre fichier dicoTest.py , rajoutez :

monBol = {"poire": 3, "pomme": 4, "orange": 2}

print("liste des fruits :")

for fruit in [monBol.keys()]{.ul}:

print(fruit)

-   Modifier la boucle for afin d'afficher à chaque passage dans la boucle la clé et la valeur associée. On doit obtenir :

poire 3

pomme 4

orange 2

-   Essayer de supprimer l'appel à la méthode keys() . Que remarquez-vous ?

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

[Remarque :]{.ul} La méthode keys() [est donc assez inutile pour parcourir les clés...]{.ul}

Les valeurs d'un dictionnaire peuvent être parcourues en utilisant la méthode values().

--- Exercice 6 ---

-   À la suite de votre travail précédent, dans le fichier dicoTest.py, ajoutez :

print("quantité de fruits :")

for nombre in [monBol.values()]{.ul}:

print(nombre)

-   Essayez de modifier la boucle afin d'obtenir le même affichage que dans le "Exercice 5". Est-ce possible ?

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Enfin, les clés et les valeurs (appelées champs) peuvent être parcourues simultanément en utilisant la méthode items().

--- Exercice 7 ---

-   Finalement, dans le fichier dicoTest.py, rajoutez à la suite de votre travail :

print("liste des fruits et quantité :")

for (fruit, nombre) in [monBol.items()]{.ul}:

print(fruit, nombre)

-   Remarquez la syntaxe de la boucle for. Avez-vous déjà vu cette syntaxe ? Rappelez dans quel contexte.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

[Remarque importante]{.ul} : un dictionnaire Python a exactement le format JSON proposé en Javascript.

Manipulation de fichiers CSV

1)  Lecture, importation et exportation de fichiers CSV

Comme vous l'avez deviné, en Python, nous allons lire les fichiers CSV grâce à des dictionnaires. De cette manière, nous allons pouvoir faire des recherches sur les contenus plutôt que sur des numéros d'indices sans aucun sens.![](images/media/image3.png){width="3.8537412510936133in" height="1.0283869203849518in"}

Toutefois, pour nous aider dans cette tâche, nous [choisissons]{.ul} de répéter les en-têtes pour chaque élément. Ainsi :

Le tableau ci-contre se notera en Python :

Table = [

{'Nom': 'Erwann', 'Français': '16', 'Science': '12', 'Histoire': '15'},\
{'Nom': 'Céline', 'Français': '14', 'Science': '16', 'Histoire': '13'}

]

Table[0] renvoie le premier enregistrement :

{'Nom': 'Erwann', 'Français': '16', 'Science': '12', 'Histoire': '15'}

[Convention]{.ul} : les fichiers CSV lus en Python se présentent toujours sous la forme d'une liste de dictionnaires dont les clés sont les en-têtes du tableau et les valeurs sont les contenus.

Nous allons tout d'abord utiliser une bibliothèque pour importer des fichiers CSV. Cette bibliothèque s'appelle... [csv]{.ul} !

--- Exercice 8 ---

-   Pour ouvrir des fichiers en Python, on utilise la commande suivante :

donnéesDuFichier = open( nomDuFichier, modeDOuverture)

avec nomDuFichier qui est un "string"

et modeDOuverture qui vaut 'r' si l'on veut lire le fichier et 'w' si on veut écrire un nouveau fichier. *Pour plus d'informations : "Google open python3"*

-   Créez un dossier appelé NSI_CSV et téléchargez le fichier exemple.csv disponible sur mon site internet : [[bouillotvincent.github.io]{.ul}](http://bouillotvincent.github.io) .

-   Dans VSCodium, créez un nouveau fichier appelé csvReader.py et, à l'aide de l'instruction [open]{.ul}, ouvrez le fichier exemple.csv.

-   En ajoutant un [print]{.ul} à votre code, affichez le contenu de exemple.csv.

-   Que pensez-vous de votre résultat ?

On ne va pas se mentir,

\<\_io.TextIOWrapper name='exemple.csv' mode='r' encoding='UTF-8'\

n'est pas super lisible et pas vraiment ce que l'on attendait...

--- Exercice 9 ---

On va importer la bibliothèque [csv]{.ul} pour pouvoir lire ce fichier que l'on vient d'ouvrir.

-   Dans votre fichier csvReader.py, importez la bibliothèque csv.

-   Recopiez la fonction importCSV(fichier, separateur) qui va nous permettre d'importer des fichiers CSV.

-   Complétez la fonction pour afficher le tableau importé à l'aide d'un [print]{.ul}, puis testez cette fonction en appelant [table = importCSV('exemple.csv')]{.ul}.

-   Que constatez-vous ? \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Caramba... Encore raté : \<csv.DictReader object at 0x10335ea90\

Toujours pas très lisible...

Pourriez-vous brièvement expliquer l'origine de cette erreur ? Dis autrement : que fait DictReader ?

--- Exercice 10 ---

Finalement, pour lire le contenu de notre objet csv.DictReader, nous allons simplement parcourir la table "tCSV" à l'aide d'une boucle for.

-   Dans la fonction importCSV, ajoutez une boucle for sur une variable que l'on appellera ligne et qui va parcourir la table "tCSV". À chaque passage dans la boucle affichez à l'aide de [print]{.ul} la valeur de la variable ligne.

-   Est-ce que tout fonctionne comme on le souhaitait initialement (p12) ? Pourquoi ? \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

--- Exercice 12 ---

Afin de conserver un certain ordre, la bibliothèque CSV utiliser une structure Python hybride entre liste et dictionnaire qui est appelée un OrderedDict.

On peut transformer cette structure en dictionnaire Python simplement en appelant dict() sur celle-ci.

-   Toujours dans la même fonction, afficher dict(ligne).

-   Est-ce que tout fonctionne comme on le souhaitait initialement (p12) ? \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

-   Finalement, modifiez votre fonction afin remplir une liste que l'on va appeler tableau et qui contiendra tous les dictionnaires que vous avez créé ligne par ligne.

[Si vous avez fini avant :]{.ul} Faites la même chose en 2 lignes : une pour importer tCSV et une pour renvoyer le résultat.

--- Exercice 13 ---

La fonction ci-dessous permet d'exporter une table au format CSV.

-   Recopiez le code ci-dessous dans votre programme csvReader.py.

-   Identifiez ce que fait chaque ligne de ce programme et écrivez-le en commentaires dans votre programme.

-   Pour vérifier votre code, exportez votre table dans le fichier que l'on nommera "exemple2.csv" , puis ouvrez ce fichier avec un tableur. Votre export a-t-il fonctionné ?

[Conclusion :]{.ul}

-   Sur un plan connaissances pures, on sait maintenant importer et exporter des fichiers CSV grâce à la librairie csv, ce qui va nous permettre de réaliser des opérations sur ces fichiers.

-   Sur un second plan, vous remarquerez que l'on a beaucoup travaillé pour faire fonctionner la librairie comme on le souhaitait. On peut se demander si on n'aurait pas été plus rapide en créant directement notre propre lecteur et écriveur de fichiers CSV (voir exercice 4!)

5)  Opérations sur les tables

--- Exercice 14 ---

On se propose de faire quelques tests dans le programme principal (PAS les fonctions) de csvReader.py .

Si vous voulez comprendre ce qu'il se passe dans la suite, je vous conseille très fortement [d'écrire]{.ul} les résultats des questions suivantes sur le cours ou une feuille que vous garderez en face de vous :

-   Qu'affiche print(table[0]) ? \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

-   À quoi vous attendez-vous si vous tapez print(table[1]) ?

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

-   Qu'affiche print(table[0]['Nom']) ? \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

-   Comment feriez-vous pour afficher la note de Céline en Science ?

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

-   Quel résultat renvoie print(table[0:1]['Nom']) ? \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\
    Que pouvez déduire de cela ?

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Nous allons maintenant étudier comment filtrer des lignes et des colonnes puis nous verrons comment trier une table en fonction d'une colonne.

--- Exercice 15 ---

Proposez une fonction filtrerLigne qui prend en argument un tableau, un critère (Nom, Science ...) ainsi qu'une valeur et renvoie une liste de dictionnaire filtrée. Seules les lignes correspondantes à votre critère d'égalité (par simplicité) doivent apparaitre.

[Si vous avez fini avant :]{.ul} Faites la même chose en 1 ligne !

Filtrer suivant une colonne s'appelle aussi une [projection]{.ul}.

--- Exercice 16 ---

On suppose pour l'instant que l'on cherche à faire notre projection sur un seul critère (le 'Nom' par exemple).

-   Compléter la fonction filtrerColonne ci-dessous. Celle-ci prend en argument un tableau et un critère (Nom, Science ...) et renvoie un tableau filtré. Seules les colonnes correspondantes à votre critère doivent apparaitre.

-   Testez votre code grâce à un print : print(filtrerColonne(table, 'Nom')) .

Conservez une seule colonne est assez inutile... On suppose à présent que l'utilisateur peut donner une liste de critères. listeCriteres sera donc de type list.

-   Modifier le programme précédent afin de prendre en compte une sélection sur une liste de critères.\
    Les modifications ne concerneront que la boucle interne indiquée par \# ici

[Si vous avez fini avant :]{.ul} Faites la même chose en 1 ligne !

Finalement, le tri d'une table suivant une colonne peut se faire grâce aux algorithmes de tri.

Dans le cadre de ce chapitre, nous allons utiliser l'instruction [sorted]{.ul} propre à Python. [sorted]{.ul} possède un argument très pratique appelé [key]{.ul} qui permet de préciser selon quel critère une liste doit être triée (cela est un objet fonction de variables à trier). Un autre argument d'intérêt est [reverse]{.ul} qui est un booléen permettant d'indiquer si on souhaite que l'ordre soit croissant (False) ou décroissant (True).

--- Exercice 17 ---

-   Recopiez le code ci-dessous :

-   Testez votre code en lançant triTable sur votre table et en classant par 'Science'. Cela fonctionne-t-il ?

On a l'impression que le tableau est correctement trié : sorted doit regarder ma liste et la trier suivant le critère ['Science']. C'est bon, on a tout compris.

Non, on n'a rien compris. C'est même le drame car un nouveau mot-clé est apparu : c'est le mot clé lambda .

"Lambda, c'est quoi ???"

[Réponse :]{.ul} Lambda est appelé "fonction anonyme" .

Lorsque l'on a besoin d'une fonction pour une utilisation unique, à la volée, dans un code, il est souvent fastidieux de créer une fonction complète. C'est là que la lambda entre en scène.

Par exemple, pour calculer un carré d'un nombre d'une manière ponctuelle :

En résumé :

C'est exactement la même chose qu'une fonction, seule la syntaxe change:

-   lambda au lieu de def ;

-   pas de nom de fonction ;

-   pas de parenthèses ;

-   pas de mot clé return.

6)  Fusion et jointure de table

[Définition :]{.ul} La fusion de deux tables consiste à ajouter des enregistrements d'une table B à ceux d'une table A, ces deux tables ayant exactement la même structure.

[Exemple :]{.ul}

On dispose de deux tables : ![](images/media/image4.png){width="2.5684142607174105in" height="1.1840780839895013in"}![](images/media/image5.png){width="2.5686898512685916in" height="0.9031900699912511in"}

La fusion de ces deux tables (ayant la même structure) va nous donner une unique table. Vous pouvez aisément voir laquelle !

--- Exercice 18 ---

[Fusion :]{.ul} cette étape est à faire en autonomie.

Nous allons travailler de manière "industrielle" en nous basant sur une librairie que nous avons déjà créé (dans notre cas, csvReader.py ). Pour faire cela, [placez-vous dans le même répertoire]{.ul} que celui où vous avez créé csvReader.py .

-   Créez un nouveau fichier appelé jointure.py et [depuis csvReader]{.ul}, [import]{.ul}ez la fonction [importCSV]{.ul}.

-   Créez une fonction verifieCle qui vérifie si les descripteurs de vos deux tables sont strictement égales. verifieCle prendra en argument d'entrée : deux tables table1 et table2 formatées de manière classique (liste de dictionnaires). verifieCle renverra True si les descripteurs des deux tables sont égaux et False sinon.

-   Créez une fonction fusion qui effectue la fusion de vos deux tables si celles-ci ont la même structure et renvoie "Tables non fusionnables" sinon. fusion prendra en argument d'entrée deux tables table1 et table2 formatées de manière classique (liste de dictionnaires). fusion renverra une table fusionnée tableFusion.

-   Finalement, la table fusionnée sera exportée au format csv grâce à l'importation de la fonction exportCSV de notre bibliothèque csvReader.

```{=html}
<!-- --
```
-   *On testera le code sur les fichiers exemple.csv et exemple3.csv à chaque étape de sa conception afin d'éviter d'être débordé(e) à la fin du développement. N'hésitez pas à modifier les fichiers csv sachant que ceux-ci sont de simples fichiers texte. Affichez la table fusionnée dans un tableur pour vérifier la correction de votre fusion.*

[Définition :]{.ul} La jointure de deux tables consiste à ajouter des enregistrements d'une table B à ceux d'une table A selon une clé commune aux deux tableaux.

[Rem]{.ul} : lors d'une jointure, on ne va pas laisser de "cases vides". On va supprimer les éléments dont certaines informations sont manquantes.

[Exemple :]{.ul}![](images/media/image6.png){width="2.1684208223972004in" height="0.9350579615048119in"}

On dispose de deux tables : ![](images/media/image4.png){width="2.5684142607174105in" height="1.1840780839895013in"}

Ces deux tables ont clairement une clé en commun. Notez que cela ne serait pas du tout une bonne clé primaire si on travaillait sur tout le lycée. La jointure de ces deux tables va nous donner une unique table :![](images/media/image7.png){width="5.277777777777778in" height="1.3612325021872267in"}

On voit que l'information sur Céline est perdue sinon, on aurait une table avec des données manquantes.

--- Exercice 19 ---

[Jointure :]{.ul} cette étape est un peu particulière.

Nous allons réutiliser le fichier jointure.py créé à l'étape 10. *On testera le code sur les fichiers exemple.csv et exempleAge.csv à chaque étape de sa conception afin d'éviter d'être débordé(e) à la fin du développement. N'hésitez pas à modifier les fichiers csv sachant que ceux-ci sont de simples fichiers texte. Affichez la table fusionnée dans un tableur pour vérifier la correction de votre fusion.*

-   Dans l'exemple ci-dessus, identifiez les étapes importantes à effectuer pour réaliser la jointure de deux tables. On essaiera d'imaginer un algorithme simple que l'on fera tourner à la main.

-   Créez une fonction jointure qui effectue la jointure de deux tables selon une clé donnée. Si la clé proposée n'est pas un descripteur du tableau, on renverra "Aucun descripteur NOM_DU_DESCRIPTEUR trouvé dans ces tables". jointure prendra en argument d'entrée deux tables table1 et table2 formatées de manière classique (liste de dictionnaires) ainsi qu'un descripteur de table. jointure renverra une table jointe tableJointe.

-   Finalement, la table jointe sera exportée au format csv grâce à l'importation de la fonction exportCSV de notre bibliothèque csvReader.

[Projet]{.ul}

On va passer sur cela cette année...

[Exercice 1 :]{.ul}

  --------- ----------------------- -------------------- -----------------------------------------
  Nom   Date de naissance   Spé abandonnée   Nombre d'élèves abandonnant la spé
  Tommy     2003                    Maths                17
  Timmy     2004                    Anglais              25
  Tammy     2003                    SI                   72
  Robert    2002                    Maths                17
  Gérard    1994                    Anglais              25
  --------- ----------------------- -------------------- -----------------------------------------

1)  Quel(s) enregistrement(s) correspond(ent) à la date de naissance 2003 ? On utilisera la notation entre crochets.

2)  En déduire ce qui manque à cette base de données.

3)  a. À l'aide de clé(s) étrangère(s), décomposez cette base de données en deux bases de données.\
    b. Dans votre nouvelle base, donnez l'enregistrement correspondant à la spé abandonnée "Maths".

[Exercice 2 :]{.ul}

On donne les différentes tables d'une base de données d'une bibliothèque :

-   Etudiant(NumEtd, NomEtd, AdresseEtd)

-   Livre(NumLivre, TitreLivre, NumAuteur, NumEditeur, NumTheme, AnneeEdition)

-   Auteur(NumAuteur, NomAuteur, AdresseAuteur)

-   Editeur(NumEditeur, NomEditeur, AdresseEditeur)

-   Theme(NumTheme, IntituléTheme)

-   Prêt(NumEtd, NumLivre, DatePret, DateRetour)

Un étudiant peut emprunter plusieurs livres à la fois. Chaque fois qu'un livre est retourné, la date de retour est mise à jour. Pour des raisons de statistique, on conserve dans une table Prêt toutes les informations sur les livres retournés.

1)  Expliquer pourquoi "NomAuteur" n'est pas une bonne clé primaire pour la table Auteur.

```{=html}
<!-- --
```
4)  Créez la table Prêt avec les conditions suivantes :\
    a. L'étudiant 1 prend le livre 3 le 12/12/2012\
    b. L'étudiant 1 prend le livre 2 le 12/12/2012\
    c. L'étudiant 3 prend le livre 3 le 15/12/2012\
    d. L'étudiant 1 reprend le livre 3 le 01/01/2013

5)  Donner les clés primaires associées aux tables de cette bibliothèque. Pour la table Prêt, on s'aidera de la question 2) et on se rappellera qu'une clé primaire doit identifier de manière unique une entrée : une clé primaire peut donc être un couple (un triplet, etc...) d'identifiants.

[Exercice 3 :]{.ul}

Réécrire le premier tableau de la page 2 sous format CSV.

[Exercice 4 : réflexion autour de l'importation de fichier CSV]{.ul}

Dans la partie sur le CSV, nous avons utilisé la bibliothèque csv. Nous pouvons également faire l'importation des fichiers csv à la main et comprendre ce qu'il se passe sous le capot de la bibliothèque csv.

L'instruction donnéesTexte.split(';') permet de découper des données textuels sous forme de liste selon un séparateur donné (ici, le ';').

[Exemple]{.ul} :

'les;marmottes;ont;le;covid'.split(';') nous donne : ['les', 'marmottes', 'ont', 'le', 'covid']

1)  Créez une fonction : def importCSVDuPauvre(fichier : str, separateur = ";") qui va importer des fichiers CSV et renvoyer une liste de dictionnaire.\
    Pour l'instant, écrivez simplement return à la fin de votre fonction.

```{=html}
<!-- --
```
6)  Dans votre fonction, ouvrez en lecture le fichier et chargez son contenu dans une variable appelée donnéesDuFichier.

7)  Grâce à une boucle sur les lignes du fichier CSV, affichez toutes les lignes une à une puis essayez de les transformer en liste... On enregistrera chaque ligne dans une variable appelée listLigne.

8)  Pour créez nos dictionnaires, nous avons besoin de deux choses : les en-têtes et les valeurs.

a. Quelle ligne du fichier CSV contient les en-têtes ? Imaginez alors une méthode (ou un test) qui vous permettrait d'obtenir l'en-tête du fichier CSV.

b. De la même manière, imaginez une manière d'organiser vos données pour que l'on obtienne des enregistrements du type ci-dessous. Pensez à la manière d'initialiser les dictionnaires :

Table = [

{'Nom': 'Erwann', 'Français': '16', 'Science': '12', 'Histoire': '15'},\
{'Nom': 'Céline', 'Français': '14', 'Science': '16', 'Histoire': '13'}

]

[Exercice 5 : réflexion libre]{.ul}

En vous aidant de l'exercice 4, créez une fonction permettant de réaliser la même opération que exportCSV (p12) i.e. exporter une liste de dictionnaires vers un fichier CSV.
